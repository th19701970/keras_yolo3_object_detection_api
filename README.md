# keras-yolo3 Object Detection API 
## Setup

Please replace `/path/to/yolo_weights.h5` and `/path/to/voc_classes.txt` by your own file.

`docker run -p 80:80 -v /path/to/yolo_weights.h5:/app/model_data/yolo_weights.h5 -v /path/to/voc_classes.txt:/app/model_data/voc_classes.txt registry.gitlab.com/sleepless-se/keras_yolo3_object_detection_api:latest`

## Test,
**Access** 
http://localhost

You can see `Welcome to keras-yolo3 Object detection!`

**Object ditection**

Send your image to the API. It's return the result of object detection by YOLOv3. 

Please replace to your image path `path/to/image.jpg`

`curl -X POST -F image=@path/to/image.jpg http://localhost/predict`


## Develop

You can edit ImageAI API. 

1. Folk [this repository](https://gitlab.com/sleepless-se/keras_yolo3_object_detection_api)
1. Clone repository `git clone git@gitlab.com:YOUR_ID/keras_yolo3_object_detection_api.git`
1. Run from `docker run -p 5000:5000 -v $(pwd):/app registry.gitlab.com/sleepless-se/keras_yolo3_object_detection_api python3 api.py` 
1. Edit `api.py` or `yolo.py`

## Build and Push

You only need push to GitLab. It's automatically build and push to GitLab Container Registry. 
